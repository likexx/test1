// Package Hello3Service provides Hello3Service logic implement
package service

import (
	"context"
	"log"

	pb "gitlab.com/likexx/test1/stub/org.test/grpc-go"
)

// Hello3Service
type Hello3Service struct{
    pb.UnimplementedHello3Server
}

func New() *Hello3Service {
	svr := &Hello3Service{
		// init custom fileds
	}
	return svr
}

// Heathcheck
func (s *Hello3Service) Heathcheck(ctx context.Context, req *pb.HealthcheckRequest) (rsp *pb.HealthcheckResponse,err error) {
	// implement business logic here ...
	// ...

	log.Printf("Received from Heathcheck request: %v", req)
	rsp = &pb.HealthcheckResponse{
		// Msg: "Hello " + req.GetMsg(),
	}
	return rsp,err
}

// Helloworld
func (s *Hello3Service) Helloworld(ctx context.Context, req *pb.HelloworldRequest) (rsp *pb.HelloworldResponse,err error) {
	// implement business logic here ...
	// ...

	log.Printf("Received from Helloworld request: %v", req)
	rsp = &pb.HelloworldResponse{
		// Msg: "Hello " + req.GetMsg(),
	}
	return rsp,err
}