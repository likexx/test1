package main

import (
	pb "gitlab.com/likexx/test1/stub/org.test/grpc-go"
	"Hello3Service/cmd/hello3service/service"
	"github.com/skema-dev/skema-go/grpcmux"
	"github.com/skema-dev/skema-go/logging"
)

func main() {
	srv := grpcmux.NewServer()
	srvImp := service.New()
	pb.RegisterHello3Server(srv, srvImp)
	
	ctx, mux, conn := srv.GetGatewayInfo()
	pb.RegisterHello3HandlerClient(ctx, mux, pb.NewHello3Client(conn))
	

	logging.Infof("Serving gRPC start...")
	if err := srv.Serve(); err != nil {
		logging.Fatalf("Serve error %v", err.Error())
	}
}
