module Hello3Service

go 1.16

require (
	github.com/skema-dev/skema-go v1.0.1
	gitlab.com/likexx/test1/stub/org.test/grpc-go v1.0.2
)
